//--------------------------------------------------------------------------------------
// File: Tessellation.fx
//
// DirectX 11 Tessellation
//--------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------

// The matrices (4x4 matrix of floats) for transforming from 3D model to 2D projection (used in vertex shader)
float4x4 WorldMatrix;
float4x4 ViewMatrix;
float4x4 ProjMatrix;
float4x4 ViewProjMatrix;
float4x4 InvViewMatrix;

// Viewport Dimensions
float ViewportWidth;
float ViewportHeight;

// Information used for lighting (in the vertex or pixel shader)
float3 Light1Pos;
float3 Light2Pos;
float3 Light1Colour;
float3 Light2Colour;
float3 AmbientColour;
float  SpecularPower;
float  Attenuation;
float3 CameraPos;

// Variable used to tint each light model to show the colour that it emits
float3 TintColour;

//**** Displacement mapping settings
float DisplacementScale; // Depth of displacement mapping - displacement in world units for the maximum height in the height map
float MinTessDistance;   // Distance at which maximum tessellation will be used (to be used in hull shader patch constant function HS_FixedConstants)
float MaxTessDistance;   // --"-- minimum
float MinTessellation;   // Minimum tessellation factor, i.e. maximum detail level (to be used in hull shader patch constant function HS_FixedConstants)
float MaxTessellation;   // --"-- maximum
float NoiseScale;
float WaterOffset;
float NormalModifier;

// Textures
Texture2D DiffuseMap;       // Diffuse texture map (with optional specular map in alpha)
Texture2D NormalHeightMap;  // Normal & height map used for displacement mapping - exactly the same map as used for parallax mapping
Texture2D GeneratedHeightMap;
Texture2D NoiseMap;
Texture2D WaterMap;
Texture2D RockMap;
Texture2D GroundMap;
Texture2D SnowMap;

// Samplers to use with the above textures
SamplerState TrilinearWrap
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState PointClamp
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
};


//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------

// This structure describes generic vertex data to be sent into the vertex shader
struct VS_INPUT
{
    float3 Pos    : POSITION;
    float3 Normal : NORMAL;
	float2 UV     : TEXCOORD0;
};

// The input required for the per-pixel lighting pixel shader, containing a 2D position, lighting colours and texture coordinates
struct PS_LIGHTING_INPUT
{
    float4 ProjPos     : SV_Position;
	float3 WorldPos    : POSITION;
	float3 WorldNormal : NORMAL;
    float2 UV          : TEXCOORD0;
};

// More basic techniques don't deal with lighting
struct PS_BASIC_INPUT
{
    float4 ProjPos : SV_Position;
    float2 UV      : TEXCOORD0;
};


//**************************************************************************************
// Tessellation - Inputs and Outputs
//**************************************************************************************

// Vertex input with tangents
struct VS_TANGENT_INPUT
{
    float3 Pos     : POSITION;
	float2 UV      : TEXCOORD0;
};

struct HS_INPUT
{
    float3 WorldPos     : POSITION;
	float2 UV           : TEXCOORD0;
	float  DistanceFactor : VERTEXDISTANCEFACTOR;
	float  VertHeight : INSIDETESSFACTOR;
};

struct HS_CONTROL_POINT_OUTPUT
{
    float3 WorldPos     : POSITION;
	float2 UV           : TEXCOORD0;
	float  DistanceFactor : VERTEXDISTANCEFACTOR;
	float  VertHeight   : INSIDETESSFACTOR;
};

struct HS_PATCH_CONSTANT_OUTPUT
{
    float    EdgeTess[4] : SV_TessFactor;
    float    InsideTess[2]  : SV_InsideTessFactor; 
};

struct DS_OUTPUT
{
    float4 ProjPos      : SV_Position;
    float3 WorldPos     : POSITION;
    float3 WorldNormal  : NORMAL;
    float2 UV           : TEXCOORD0;
};


//**************************************************************************************
// Tessellation - Vertex Shader
//**************************************************************************************

// Vertex shader for tessellation is just the same as the usual vertex shader except it only transforms the model geometry into world space,
// hull shader works in world space, and the domain shader handles the transformation into screen coordinates (with the view and projection matrix)
HS_INPUT VS_QuadTessellation( VS_TANGENT_INPUT vIn )
{
	HS_INPUT vOut;

	// Transform the input model vertex position into world space
	float4 modelPos = float4(vIn.Pos, 1.0f);
	vOut.WorldPos = mul( modelPos, WorldMatrix ).xyz;

	//Get how high the vertex will be and pass it through to change the tess factor
	float4 NormalHeight = GeneratedHeightMap.SampleLevel(TrilinearWrap, vIn.UV, 0);

	vOut.VertHeight = NormalHeight.r;

	float3 vertexPos = vOut.WorldPos.xyz;
	vertexPos.y += DisplacementScale * vOut.VertHeight;

	float vertexDistance = distance(vertexPos, CameraPos);

	vOut.DistanceFactor = saturate((vertexDistance - MinTessDistance) / (MaxTessDistance - MinTessDistance));

	// Pass texture coordinates (UVs) on unchanged
	vOut.UV = vIn.UV;

	return vOut;
}


//**************************************************************************************
// Tessellation - Hull Shader (and patch constant function)
//**************************************************************************************

// The Hull Shader reads in a set of "patches", surfaces defined by "control points". The hull shader is called once for each control point, and its job is to
// do any kind of manipulation or calculation of the control points required *prior* to the tessellation
// A patch is some kind of surface, which starts as a line, triangle or quad, but which will be tessellated and curved/bent into any shape we wish based on the
// control points. Exactly how the control points define the shape of the patch is up to the programmer. For example the control points might be used to take a
// quad and manipulate it as a Bezier spline. However for the case of displacement mapping we won't use splines, the control points are just the vertices of each
// triangle, and we're not going to manipulate them prior to tessellation, so there is little for this hull shader to do yet. However, we need to define a hull
// shader in any case - the most important part being the sequence of attributes that are written before the shader itself:
//
[domain("quad")]                        // The input before the tessellation is triangles
[partitioning("fractional_even")]       // Tessellation style to use - can use "fractional_even", "fractional_odd", "integer" or "pow2"
[outputtopology("triangle_cw")]        // We want the tessellated output to be triangles too
[outputcontrolpoints(4)]      // Not using splines or similar (see lecture) in this case the input and output "control points" are just the vertices of each triangle
[patchconstantfunc("HS_DistanceTess")] // Name of the "patch constant" function used to select the tessellation factors etc. (see the function after this one)
[maxtessfactor(64.0)]                  // Maximum tessellation factor that the constant function (named on the line above) will select
HS_CONTROL_POINT_OUTPUT HS_QuadTessellation
(
	InputPatch<HS_INPUT, 4> inputPatch,    // The input patch is an array of control points (up to 32 per patch), here simply the 3 vertices of each triangle
	uint i : SV_OutputControlPointID,
	uint PatchID : SV_PrimitiveID
)
{
    HS_CONTROL_POINT_OUTPUT hOut;
    
    // Just copy current control point to the output. The Domain Shader will do the displacement mapping after receiving the output of the tessellation stage
	hOut.WorldPos		= inputPatch[i].WorldPos;
	hOut.UV				= inputPatch[i].UV;
	hOut.DistanceFactor = inputPatch[i].DistanceFactor;
	hOut.VertHeight     = inputPatch[i].VertHeight;

    return hOut;
}

// The "patch constant function" used for the hull shader above. All hull shaders require such a function. It is called once for each patch (rather than once
// for each control point) and sets up any values that are constant for the entire patch. In this example it is called once for each triangle in our mesh.
// At a minimum the function must specify the "Tessellation factors": these determine how finely the patches (triangles) will be tessellated
// For a triangle patch, we specify one tessellation factor for each edge (how many times the edges will be split), and one factor for the inside of the
// triangle (how many extra triangles will be generated in the interior of the triangle)
// Using the same tessellation factor for everything is simplest and will create a nice regular tessellation. However, if you are using any kind of varying
// tessellation (e.g. tessellation that decreases with distance), then the edges between triangles of different tessellation will need to have their factors
// matched or you will see cracks in the geometry
HS_PATCH_CONSTANT_OUTPUT HS_DistanceTess
(
	// This function can read the input patches and/or the updated patches that wer output from the hull shader above. Here we will read the output of the
	// hull shader as it will help with calculating distance-based tessellation factors in one of the later lab exercises
	OutputPatch<HS_CONTROL_POINT_OUTPUT, 4> outputPatch
)
{
	HS_PATCH_CONSTANT_OUTPUT hOut;

	float edgeTess0 = lerp( MaxTessellation, MinTessellation, max( outputPatch[0].DistanceFactor, outputPatch[3].DistanceFactor));
	float edgeTess1 = lerp( MaxTessellation, MinTessellation, max( outputPatch[1].DistanceFactor, outputPatch[0].DistanceFactor));
	float edgeTess2 = lerp( MaxTessellation, MinTessellation, max( outputPatch[2].DistanceFactor, outputPatch[1].DistanceFactor));
	float edgeTess3 = lerp( MaxTessellation, MinTessellation, max( outputPatch[3].DistanceFactor, outputPatch[2].DistanceFactor));

	float height0 = clamp(abs(outputPatch[3].VertHeight - outputPatch[0].VertHeight) * DisplacementScale, 0.1f, 2.0);
	float height1 = clamp(abs(outputPatch[0].VertHeight - outputPatch[1].VertHeight) * DisplacementScale, 0.1f, 2.0);
	float height2 = clamp(abs(outputPatch[1].VertHeight - outputPatch[2].VertHeight) * DisplacementScale, 0.1f, 2.0);
	float height3 = clamp(abs(outputPatch[2].VertHeight - outputPatch[3].VertHeight) * DisplacementScale, 0.1f, 2.0);


	hOut.EdgeTess[0] = clamp(edgeTess0 * height0, MinTessellation, MaxTessellation);
	hOut.EdgeTess[1] = clamp(edgeTess1 * height1, MinTessellation, MaxTessellation);
	hOut.EdgeTess[2] = clamp(edgeTess2 * height2, MinTessellation, MaxTessellation);
	hOut.EdgeTess[3] = clamp(edgeTess3 * height3, MinTessellation, MaxTessellation);

	float insideTess = max(max(max(hOut.EdgeTess[0], hOut.EdgeTess[1]), hOut.EdgeTess[2]), hOut.EdgeTess[3]);

	hOut.InsideTess[0] = insideTess;
	hOut.InsideTess[1] = insideTess;


    return hOut;
}



//**************************************************************************************
// Tessellation - Domain Shader
//**************************************************************************************

// The domain shader gets the output points from the hull shader, which in our case is just the original geometry triangles in world space, and it
// also gets a tessellated patch in a generic space with 0->1 coordinates from the tessellator stage. It combines these together to create a
// tessellated patch in world space, which it then finally transforms to 2D projection space for rendering by the pixel shader.
//
// When working with quad patches the generic tessellation arrives in a 0->1 unit square, a bit like texture coordinates. However, when working with
// triangle patches the generic tessellation arrives as three Barycentric coordinates. Barycentric coordinates are a good way to specify a position
// within an arbitrary triangle. If the triangle points are P0,P1 and P2, then the Barycentric coordinates a0,a1,a2 define the point:
//  P = a0 * P0 + a1 * P1 + a2 * P2
// In other words add together the points (or normals or whatever) with the Barycentric coordinates as weightings. The one rule for Barycentric
// coordinates is that a0 + a1 + a2 will always be 1. With this rule you can guarantee the point represented is within the plane of the triangle.
// Effectively Barycentric coordinates define a coordinate system with one triangle point as the origin and the two radiating edges as axes.
// Barycentric coordinates occasionally crop up in graphics (or physics) when defining points in triangles, although new, they are not at all complex
// to use. See the Van Verth maths text for further information
[domain("quad")]
DS_OUTPUT DS_DisplacementMapping
(
	HS_PATCH_CONSTANT_OUTPUT input,
	float2 uv : SV_DomainLocation,
    const OutputPatch<HS_CONTROL_POINT_OUTPUT, 4> QuadPatch
)
{
    DS_OUTPUT dOut;

	float3 bottom = lerp(QuadPatch[0].WorldPos, QuadPatch[1].WorldPos, uv.x);
	float3 top = lerp(QuadPatch[3].WorldPos, QuadPatch[2].WorldPos, uv.x);
	float3 result = lerp(bottom, top, uv.y);

	dOut.WorldPos = result;

	float2 bottomUV = lerp(QuadPatch[0].UV, QuadPatch[1].UV, uv.x);
	float2 topUV = lerp(QuadPatch[3].UV, QuadPatch[2].UV, uv.x);
	float2 resultUV = lerp(bottomUV, topUV, uv.y);

	dOut.UV = resultUV;

	//////////////////////////////
	//////////////////////////////

	// Get the height of this pixel and all pixels around it

	float n = GeneratedHeightMap.SampleLevel(TrilinearWrap, resultUV, 0, int2(0, 1)).r * DisplacementScale;
	float s = GeneratedHeightMap.SampleLevel(TrilinearWrap, resultUV, 0, int2(0, -1)).r * DisplacementScale;
	float e = GeneratedHeightMap.SampleLevel(TrilinearWrap, resultUV, 0, int2(1, 0)).r * DisplacementScale;
	float w = GeneratedHeightMap.SampleLevel(TrilinearWrap, resultUV, 0, int2(-1, 0)).r * DisplacementScale;

	// Cross the vector of the pixels around to get the normal
	float3 ns = float3(0.0f, (s - n), NormalModifier);
	float3 ew = float3(NormalModifier, (w - e), 0.0f);

	dOut.WorldNormal = normalize(cross(ew, ns));

	float displacement = GeneratedHeightMap.SampleLevel(TrilinearWrap, resultUV, 0).r;

	dOut.WorldPos.y += displacement * DisplacementScale;

	float4 Noise = NoiseMap.SampleLevel(TrilinearWrap, dOut.UV * dOut.WorldPos.x * dOut.WorldPos.y * resultUV.x, 0);

	//Offset the x and z by -0.5 so it scales in both positive and negative, but not by as much
	//This keeps the general shape a lot closer
	dOut.WorldPos.x += (Noise.x - 0.5f) * NoiseScale;
	dOut.WorldPos.z += (Noise.z - 0.5f) * NoiseScale;


    // Transform world position with view-projection matrix
    dOut.ProjPos = mul( float4( dOut.WorldPos, 1.0f ), ViewProjMatrix );

    return dOut;
}


//**************************************************************************************
// Tessellation - Pixel Shader
//**************************************************************************************

// The pixel shader for displacement mapping is essentially the same as normal mapping. We have created extra tessellated geometry and displaced the positions
// to create a real bumpy surface. We used a height map to get the geometry displacements, so just use an associated normal map to get the normals.
float4 PS_NormalMapping( DS_OUTPUT pIn ) : SV_Target
{
	// Can't guarantee the normals are length 1 now (because the world matrix may contain scaling), so renormalise
	// If lighting in the pixel shader, this is also because the interpolation from vertex shader to pixel shader will also rescale normals
	float3 worldNormal = normalize(pIn.WorldNormal);


	///////////////////////
	// Calculate lighting

	// Calculate direction of camera
	float3 CameraDir = normalize(CameraPos - pIn.WorldPos.xyz); // Position of camera - position of current vertex (or pixel) (in world space)

	//// LIGHT 1
	float3 Light1Dir = normalize(Light1Pos - pIn.WorldPos.xyz);   // Direction for each light is different
	float Light1Dist = length(Light1Pos - pIn.WorldPos.xyz);
	float3 DiffuseLight1 = Light1Colour * saturate(dot(worldNormal.xyz, Light1Dir)) / Light1Dist * Attenuation;
	float3 halfway = normalize(Light1Dir + CameraDir);
	float3 SpecularLight1 = DiffuseLight1 * pow(max(dot(worldNormal.xyz, halfway), 0), SpecularPower);

	//// LIGHT 2
	/*float3 Light2Dir = normalize(Light2Pos - pIn.WorldPos.xyz);
	float Light2Dist = length(Light2Pos - pIn.WorldPos.xyz);
	float3 DiffuseLight2 = Light2Colour * saturate(dot(worldNormal.xyz, Light2Dir)) / Light2Dist * Attenuation;
	halfway = normalize(Light2Dir + CameraDir);
	float3 SpecularLight2 = DiffuseLight2 * pow(max(dot(worldNormal.xyz, halfway), 0), SpecularPower);*/

	// Sum the effect of the two lights - add the ambient at this stage rather than for each light (or we will get twice the ambient level)
	float3 DiffuseLight = AmbientColour + DiffuseLight1;// + DiffuseLight2;
	float3 SpecularLight = SpecularLight1;// + SpecularLight2;

	////////////////////
	// Sample texture

	// Extract diffuse material colour for this pixel from a texture
	float4 DiffuseMaterial = float4(0.0f, 0.0f, 0.0f, 1.0f);	//Texture starts black
	float snowDifference = 0.84f - (clamp(sin(pIn.UV.y * 64.0f), -0.6f, 1.0f) * 0.04f);	//The difference between the snow and the bottom of the blended textures
	float rockDifference = snowDifference + (clamp(sin(pIn.UV.x * 64.0f), -0.6f, -0.3) * 0.2f); //The difference between the rock and the bottom of the normal textures

	//Water, doesn't add to anything (maybe sand later)
	if (pIn.WorldPos.y < 1.0f)
	{
		DiffuseMaterial = WaterMap.Sample(TrilinearWrap, (pIn.UV * 48.0f + WaterOffset));
	}
	// Definitely snow above this
	else if (pIn.WorldPos.y > 0.88f * DisplacementScale)
	{
		DiffuseMaterial = SnowMap.Sample(TrilinearWrap, (pIn.UV * 64.0f));
	}
	// Snow, adds to other bits at the bottom of the snow
	else if (pIn.WorldPos.y > snowDifference * DisplacementScale)
	{
		// Add the gradient from the snowmap to another map
		float snowWeight = (pIn.WorldPos.y / DisplacementScale - snowDifference) * (1.0f / (0.88f - snowDifference));
		//DiffuseMaterial = float4(1.0f, 0.0f, 1.0f, 1.0f);
		DiffuseMaterial += SnowMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * snowWeight;
		DiffuseMaterial += RockMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * (1.0f - snowWeight);
	}
	else if (pIn.WorldPos.y > rockDifference * DisplacementScale)
	{
		// Add the gradient from the snowmap to another map
		float rockWeight = (pIn.WorldPos.y / DisplacementScale - rockDifference) * (1.0f / (snowDifference - rockDifference));
		//DiffuseMaterial = float4(1.0f, 0.0f, 0.0f, 1.0f);
		DiffuseMaterial += RockMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * rockWeight;

		if (worldNormal.y > -0.45f)
		{
			DiffuseMaterial += RockMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * (1.0f - rockWeight);
		}
		else if (worldNormal.y > -0.55f)
		{
			float groundWeight = (worldNormal.y + 0.55f) * (1.0f / 0.1f);
			DiffuseMaterial += RockMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * groundWeight * (1.0f - rockWeight);
			DiffuseMaterial += GroundMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * (1.0f - groundWeight) * (1.0f - rockWeight);
		}
		else
		{
			DiffuseMaterial += GroundMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * (1.0f - rockWeight);
		}
	}
	else if (worldNormal.y > -0.45f)
	{
		DiffuseMaterial = RockMap.Sample(TrilinearWrap, (pIn.UV * 64.0f));
	}
	else if (worldNormal.y > -0.55f)
	{
		float rockWeight = (worldNormal.y + 0.55f) * (1.0f / 0.1f);
		DiffuseMaterial += RockMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * rockWeight;
		DiffuseMaterial += GroundMap.Sample(TrilinearWrap, (pIn.UV * 64.0f)) * (1.0f - rockWeight);
	}
	else
	{
		DiffuseMaterial += GroundMap.Sample(TrilinearWrap, (pIn.UV * 64.0f));
	}

	// Assume specular material colour is white (i.e. highlights are a full, untinted reflection of light)
	float3 SpecularMaterial = 1.0f;


	////////////////////
	// Combine colours 

	// Combine maps and lighting for final pixel colour
	float4 combinedColour;
	combinedColour.rgb = DiffuseMaterial.rgb * DiffuseLight + SpecularMaterial * SpecularLight;
	combinedColour.a = 1.0f; // No alpha processing in this shader, so just set it to 1

	//combinedColour = float4(0.0f, abs(worldNormal.y), 0.0f, 1.0f);
	//combinedColour = float4(abs(worldNormal.x), abs(worldNormal.y), abs(worldNormal.z), 1.0f);

	return combinedColour;
}



//--------------------------------------------------------------------------------------
// Other Vertex Shaders
//--------------------------------------------------------------------------------------

// This vertex shader passes on the vertex position and normal to the pixel shader for per-pixel lighting
PS_LIGHTING_INPUT VS_PixelLitTex( VS_INPUT vIn )
{
	PS_LIGHTING_INPUT vOut;

	// Use world matrix passed from C++ to transform the input model vertex position into world space
	float4 modelPos = float4(vIn.Pos, 1.0f); // Promote to 1x4 so we can multiply by 4x4 matrix, put 1.0 in 4th element for a point (0.0 for a vector)
	float4 worldPos = mul( modelPos, WorldMatrix );
	vOut.WorldPos = worldPos.xyz;

	// Use camera matrices to further transform the vertex from world space into view space (camera's point of view) and finally into 2D "projection" space for rendering
	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = mul( viewPos,  ProjMatrix );

	// Transform the vertex normal from model space into world space (almost same as first lines of code above)
	float4 modelNormal = float4(vIn.Normal, 0.0f); // Set 4th element to 0.0 this time as normals are vectors
	vOut.WorldNormal = mul( modelNormal, WorldMatrix ).xyz;

	// Pass texture coordinates (UVs) on to the pixel shader, the vertex shader doesn't need them
	vOut.UV = vIn.UV;

	return vOut;
}


// Basic vertex shader to transform 3D model vertices to 2D and pass UVs to the pixel shader
PS_BASIC_INPUT VS_BasicTransformTex( VS_INPUT vIn )
{
	PS_BASIC_INPUT vOut;
	
	// Use world matrix passed from C++ to transform the input model vertex position into world space
	float4 modelPos = float4(vIn.Pos, 1.0f); // Promote to 1x4 so we can multiply by 4x4 matrix, put 1.0 in 4th element for a point (0.0 for a vector)
	float4 worldPos = mul( modelPos, WorldMatrix );
	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = float4(mul( viewPos,  ProjMatrix ));
	
	// Pass texture coordinates (UVs) on to the pixel shader
	vOut.UV = vIn.UV;

	return vOut;
}



//--------------------------------------------------------------------------------------
// Other Pixel Shaders
//--------------------------------------------------------------------------------------

// Pixel shader that calculates per-pixel lighting and combines with diffuse and specular map
//
float4 PS_PixelLitDiffuseMap( PS_LIGHTING_INPUT pIn ) : SV_Target  // The ": SV_Target" bit just indicates that the returned float4 colour goes to the render target (i.e. it's a colour to render)
{
	// Can't guarantee the normals are length 1 now (because the world matrix may contain scaling), so renormalise
	// If lighting in the pixel shader, this is also because the interpolation from vertex shader to pixel shader will also rescale normals
	float3 worldNormal = normalize(pIn.WorldNormal); 


	///////////////////////
	// Calculate lighting

	// Calculate direction of camera
	float3 CameraDir = normalize(CameraPos - pIn.WorldPos.xyz); // Position of camera - position of current vertex (or pixel) (in world space)
	
	//// LIGHT 1
	float3 Light1Dir = normalize(Light1Pos - pIn.WorldPos.xyz);   // Direction for each light is different
	float3 Light1Dist = length(Light1Pos - pIn.WorldPos.xyz); 
	float3 DiffuseLight1 = Light1Colour * max( dot(worldNormal.xyz, Light1Dir), 0 ) / Light1Dist;
	float3 halfway = normalize(Light1Dir + CameraDir);
	float3 SpecularLight1 = DiffuseLight1 * pow( max( dot(worldNormal.xyz, halfway), 0 ), SpecularPower );

	//// LIGHT 2
	float3 Light2Dir = normalize(Light2Pos - pIn.WorldPos.xyz);
	float3 Light2Dist = length(Light2Pos - pIn.WorldPos.xyz);
	float3 DiffuseLight2 = Light2Colour * max( dot(worldNormal.xyz, Light2Dir), 0 ) / Light2Dist;
	halfway = normalize(Light2Dir + CameraDir);
	float3 SpecularLight2 = DiffuseLight2 * pow( max( dot(worldNormal.xyz, halfway), 0 ), SpecularPower );

	// Sum the effect of the two lights - add the ambient at this stage rather than for each light (or we will get twice the ambient level)
	float3 DiffuseLight = AmbientColour + DiffuseLight1 + DiffuseLight2;
	float3 SpecularLight = SpecularLight1 + SpecularLight2;


	////////////////////
	// Sample texture

	// Extract diffuse material colour for this pixel from a texture
	float4 DiffuseMaterial = DiffuseMap.Sample( TrilinearWrap, pIn.UV );
	
	// Assume specular material colour is white (i.e. highlights are a full, untinted reflection of light)
	float3 SpecularMaterial = DiffuseMaterial.a;

	
	////////////////////
	// Combine colours 
	
	// Combine maps and lighting for final pixel colour
	float4 combinedColour;
	combinedColour.rgb = DiffuseMaterial.rgb * DiffuseLight + SpecularMaterial * SpecularLight;
	combinedColour.a = 1.0f; // No alpha processing in this shader, so just set it to 1

	return combinedColour;
}


// A pixel shader that just tints a (diffuse) texture with a fixed colour
//
float4 PS_TintDiffuseMap( PS_BASIC_INPUT pIn ) : SV_Target
{
	// Extract diffuse material colour for this pixel from a texture
	float4 diffuseMapColour = DiffuseMap.Sample( TrilinearWrap, pIn.UV );

	// Tint by global colour (set from C++)
	diffuseMapColour.rgb *= TintColour / 10;

	return diffuseMapColour;
}


//--------------------------------------------------------------------------------------
// HeightBlurring
//--------------------------------------------------------------------------------------

// Can't change this one, it's the input
struct VS_BLUR_INPUT
{
	float3 Pos     : POSITION;
	float2 UV      : TEXCOORD0;
};

struct HS_BLUR_INPUT
{
	float2 UV           : TEXCOORD0;
};

struct HS_BLUR_CONTROL_POINT_OUTPUT
{
	float2 UV           : TEXCOORD0;
};

struct HS_BLUR_PATCH_CONSTANT_OUTPUT
{
	float    EdgeTess[4] : SV_TessFactor;
	float    InsideTess[2]  : SV_InsideTessFactor;
};

struct DS_BLUR_OUTPUT
{
	float4 ProjPos      : SV_Position;
	float  Displacement : TEXCOORD0;
};

HS_BLUR_INPUT VS_HeightBlurring(VS_BLUR_INPUT vIn)
{
	HS_BLUR_INPUT vOut;

	vOut.UV = vIn.UV;

	return vOut;
}


[domain("quad")]
[partitioning("fractional_even")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("HS_Const_HeightBlur")]
[maxtessfactor(64.0)]
HS_BLUR_CONTROL_POINT_OUTPUT HS_HeightBlurring
(
InputPatch<HS_BLUR_INPUT, 4> inputPatch,
uint i : SV_OutputControlPointID
)
{
	HS_BLUR_CONTROL_POINT_OUTPUT hOut;

	hOut.UV = inputPatch[i].UV;

	return hOut;
}

HS_BLUR_PATCH_CONSTANT_OUTPUT HS_Const_HeightBlur
(
OutputPatch<HS_BLUR_CONTROL_POINT_OUTPUT, 4> outputPatch
)
{
	HS_BLUR_PATCH_CONSTANT_OUTPUT hOut;

	hOut.EdgeTess[0] = MaxTessellation;
	hOut.EdgeTess[1] = MaxTessellation;
	hOut.EdgeTess[2] = MaxTessellation;
	hOut.EdgeTess[3] = MaxTessellation;

	hOut.InsideTess[0] = MaxTessellation;
	hOut.InsideTess[1] = MaxTessellation;


	return hOut;
}


[domain("quad")]
DS_BLUR_OUTPUT DS_HeightBlurring
(
HS_BLUR_PATCH_CONSTANT_OUTPUT input,
float2 uv : SV_DomainLocation,
const OutputPatch<HS_BLUR_CONTROL_POINT_OUTPUT, 4> QuadPatch
)
{
	DS_BLUR_OUTPUT dOut;

	float2 bottomUV = lerp(QuadPatch[0].UV, QuadPatch[1].UV, uv.x);
	float2 topUV = lerp(QuadPatch[3].UV, QuadPatch[2].UV, uv.x);
	float2 resultUV = lerp(bottomUV, topUV, uv.y);


	// Guassian Blur the heights to get a smoother height curve
	// Use this:
	// http://dev.theomader.com/gaussian-kernel-calculator/
	// To calculate the weights

	static const int samples = 5;
	static const float PixelOffset[samples] =
	{
		-2,
		-1,
		0,
		1,
		2,
	};

	static const float BlurWeights[samples] =
	{
		0.06136,
		0.24477,
		0.38774,
		0.24477,
		0.06136,
	};


	float displacement = 0;

	[unroll]
	for (int i = 0; i < samples; i++)
	{
		[unroll]
		for (int j = 0; j < samples; j++)
		{
			displacement += NormalHeightMap.SampleLevel(TrilinearWrap, resultUV, 0, int2(PixelOffset[i], PixelOffset[j])).w * BlurWeights[i] * BlurWeights[j];
		}
	}


	dOut.Displacement = displacement;

	// Transform world position with view-projection matrix
	dOut.ProjPos = float4(float2((resultUV - 0.5f) * 2.0f), 0.0f, 1.0f);

	return dOut;
}

float4 PS_HeightBlurring(DS_BLUR_OUTPUT pIn) : SV_Target
{
	float4 colour = float4(pIn.Displacement, 0.0f, 0.0f, 1.0f);
	return colour;
}

//--------------------------------------------------------------------------------------
// Random height map genertaion, uses Bezier Patches
//--------------------------------------------------------------------------------------

struct S_BEZIER
{
	float3 Pos     : POSITION;
	float2 UV      : TEXCOORD0;
};
struct S_VS_BEZIER
{
	float4 Pos     : POSITION;
	float2 UV      : TEXCOORD0;
};

struct HS_BEZIER_PATCH_CONSTANT_OUTPUT
{
	float    EdgeTess[4] : SV_TessFactor;
	float    InsideTess[2]  : SV_InsideTessFactor;
};

struct DS_BEZIER_OUTPUT
{
	float4 ProjPos      : SV_Position;
	float  Height		: TEXCOORD0;
};

float4 BernsteinBasis(float t)
{
	float invT = 1.0f - t;

	return float4(invT * invT * invT,
		3.0f * t * invT * invT,
		3.0f * t * t * invT,
		t * t * t);
}

S_VS_BEZIER VS_HeightBezier(S_BEZIER vIn)
{
	S_VS_BEZIER vOut;

	float4 pos = float4(vIn.UV.x, vIn.Pos.x, vIn.UV.y, 1.0f);
	vOut.Pos = pos;
	vOut.UV = vIn.UV;

	return vOut;
}

[domain("quad")]
[partitioning("fractional_even")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(16)]
[patchconstantfunc("HS_Const_Bezier")]
[maxtessfactor(64.0)]
S_VS_BEZIER HS_HeightBezier
(
InputPatch<S_VS_BEZIER, 16> inputPatch,
uint i : SV_OutputControlPointID
)
{
	S_VS_BEZIER hOut;

	hOut.Pos = inputPatch[i].Pos;
	hOut.UV = inputPatch[i].UV;

	return hOut;
}

HS_BEZIER_PATCH_CONSTANT_OUTPUT HS_Const_Bezier
(
OutputPatch<S_VS_BEZIER, 16> outputPatch
)
{
	HS_BLUR_PATCH_CONSTANT_OUTPUT hOut;

	hOut.EdgeTess[0] = MaxTessellation;
	hOut.EdgeTess[1] = MaxTessellation;
	hOut.EdgeTess[2] = MaxTessellation;
	hOut.EdgeTess[3] = MaxTessellation;

	hOut.InsideTess[0] = MaxTessellation;
	hOut.InsideTess[1] = MaxTessellation;


	return hOut;
}


[domain("quad")]
DS_BEZIER_OUTPUT DS_HeightBezier
(
HS_BLUR_PATCH_CONSTANT_OUTPUT input,
float2 uv : SV_DomainLocation,
const OutputPatch<S_VS_BEZIER, 16> Patch
)
{
	DS_BEZIER_OUTPUT dOut;

	float4 BasisU = BernsteinBasis(uv.x);
	float4 BasisV = BernsteinBasis(uv.y);

	float3 Value = float3(0, 0, 0);

	Value =  BasisV.x * (Patch[ 0].Pos * BasisU.x + Patch[ 1].Pos * BasisU.y + Patch[ 2].Pos * BasisU.z + Patch[ 3].Pos * BasisU.w);
	Value += BasisV.y * (Patch[ 4].Pos * BasisU.x + Patch[ 5].Pos * BasisU.y + Patch[ 6].Pos * BasisU.z + Patch[ 7].Pos * BasisU.w);
	Value += BasisV.z * (Patch[ 8].Pos * BasisU.x + Patch[ 9].Pos * BasisU.y + Patch[10].Pos * BasisU.z + Patch[11].Pos * BasisU.w);
	Value += BasisV.w * (Patch[12].Pos * BasisU.x + Patch[13].Pos * BasisU.y + Patch[14].Pos * BasisU.z + Patch[15].Pos * BasisU.w);

	dOut.Height = saturate((Value.y - 0.4f) * 1.5f);
	// Transform world position with view-projection matrix
	dOut.ProjPos = float4((Value.x - 0.5f) * 2.0f, (Value.z - 0.5f) * 2.0f, 0.0f, 1.0f);

	return dOut;
}

float4 PS_HeightBezier(DS_BEZIER_OUTPUT pIn) : SV_Target
{
	float4 colour = float4(0.0f, 0.0f, 0.0f, pIn.Height / 4.0f);
	return colour;
}

//--------------------------------------------------------------------------------------
// States
//--------------------------------------------------------------------------------------

// States are needed to switch between additive blending for the lights and no blending for other models

RasterizerState CullNone  // Cull none of the polygons, i.e. show both sides
{
	CullMode = None;
	FillMode = SOLID;
};
RasterizerState CullBack  // Cull back side of polygon - normal behaviour, only show front of polygons
{
	CullMode = Back;
	FillMode = SOLID;
};
RasterizerState Wireframe
{
	CullMode = None;
	FillMode = WIREFRAME;
};



DepthStencilState DepthWritesOff // Don't write to the depth buffer - polygons rendered will not obscure other polygons
{
	DepthFunc      = LESS;
	DepthWriteMask = ZERO;
};
DepthStencilState DepthWritesOn  // Write to the depth buffer - normal behaviour 
{
	DepthFunc      = LESS;
	DepthWriteMask = ALL;
};
DepthStencilState DisableDepth   // Disable depth buffer entirely
{
	DepthFunc      = ALWAYS;
	DepthWriteMask = ZERO;
};


BlendState NoBlending // Switch off blending - pixels will be opaque
{
    BlendEnable[0] = FALSE;
};
BlendState AdditiveBlending // Additive blending is used for lighting effects
{
    BlendEnable[0] = TRUE;
    SrcBlend = ONE;
    DestBlend = ONE;
    BlendOp = ADD;
};
BlendState ColourBlending
{
	BlendEnable[0] = TRUE;
	SrcBlend = ZERO;
	DestBlend = ZERO;
	BlendOp = ADD;
	SrcBlendAlpha = ONE;
	DestBlendAlpha = ONE;
	BlendOpAlpha = ADD;
};
BlendState AlphaBlending // Alpha blending is used for the particles
{
    BlendEnable[0] = TRUE;
	SrcBlend = ONE;
	DestBlend = ONE;
    BlendOp = ADD;
};


//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------

// Techniques are used to render models in our scene. They select a combination of vertex, geometry and pixel shader from those provided above. Can also set states.

// Displacement mapping using tessellation
technique11 DisplacementMapping
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS_QuadTessellation() ) );
        SetHullShader( CompileShader( hs_5_0, HS_QuadTessellation() ) );
        SetDomainShader( CompileShader( ds_5_0, DS_DisplacementMapping() ) );
        SetGeometryShader( NULL );                                   
		SetPixelShader(CompileShader(ps_5_0, PS_NormalMapping()));

		// Switch off blending states
		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullBack ); 
		SetDepthStencilState( DepthWritesOn, 0 );
	}
}

// Displacement mapping using tessellation
technique11 DisplacementMappingWireframe
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS_QuadTessellation() ) );
        SetHullShader( CompileShader( hs_5_0, HS_QuadTessellation() ) );
        SetDomainShader( CompileShader( ds_5_0, DS_DisplacementMapping() ) );
        SetGeometryShader( NULL );                                   
		SetPixelShader(CompileShader(ps_5_0, PS_NormalMapping()));

		// Switch off blending states
		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( Wireframe ); 
		SetDepthStencilState( DepthWritesOn, 0 );
	}
}


// Per-pixel lighting with diffuse map
technique11 PixelLitTex
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS_PixelLitTex() ) );
        SetHullShader( NULL );                                   
        SetDomainShader( NULL );                                   
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_5_0, PS_PixelLitDiffuseMap() ) );

		// Switch off blending states
		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullBack ); 
		SetDepthStencilState( DepthWritesOn, 0 );
	}
}


// Additive blended texture. No lighting, but uses a global colour tint. Used for light models
technique11 AdditiveTexTint
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS_BasicTransformTex() ) );
        SetHullShader( NULL );                                   
        SetDomainShader( NULL );                                   
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_5_0, PS_TintDiffuseMap() ) );

		SetBlendState( AdditiveBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullNone ); 
		SetDepthStencilState( DepthWritesOff, 0 );
     }
}


// Technique to generate gaussian blurred heights
technique11 HeightBlurring
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS_HeightBlurring()));
		SetHullShader(CompileShader(hs_5_0, HS_HeightBlurring()));
		SetDomainShader(CompileShader(ds_5_0, DS_HeightBlurring()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS_HeightBlurring()));

		// Switch off blending states
		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DepthWritesOff, 0);
	}
}

// Technique to generate a height map based off some random numbers
technique11 HeightGeneration
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS_HeightBezier()));
		SetHullShader(CompileShader(hs_5_0, HS_HeightBezier()));
		SetDomainShader(CompileShader(ds_5_0, DS_HeightBezier()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS_HeightBezier()));

		// Switch off blending states
		SetBlendState(ColourBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DepthWritesOff, 0);
	}
}