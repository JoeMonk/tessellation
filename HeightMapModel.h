#ifndef __HEIGHTMAPMODEL
#define __HEIGHTMAPMODEL

#include "Defines.h"
#include <vector>

class CHeightMapModel
{
private:

	D3DXVECTOR3                 m_Position;
	D3DXVECTOR3                 m_Rotation;
	D3DXVECTOR3                 m_Scale;

	D3DXMATRIXA16               m_WorldMatrix;

	float                       m_DisplacementAmount;

	ID3D11Buffer*			    m_pQuadVertexBuffer;
	ID3D11Buffer*			    m_pQuadIndexBuffer;
	ID3D11InputLayout*		    m_pQuadVertexLayout;
	float			            m_GridWidth;
	unsigned int				m_GridX;
	unsigned int				m_GridY;

	ID3D11ShaderResourceView*   m_pHeightMap;

	unsigned int                m_VectorSize;

public:

	CHeightMapModel(ID3DX11EffectTechnique* tessellationTechnique);
	~CHeightMapModel();
	bool LoadMaps(LPCSTR heightMap);
	bool LoadMaps(ID3D11ShaderResourceView* heightMap);
	bool SetMaps(ID3DX11EffectShaderResourceVariable* HeightMapVar);
	bool CreateModel(unsigned int gridX, unsigned int gridY, float width, float maxTessellation);
	bool Render(ID3DX11EffectTechnique* tessellationTechnique);
	void UpdateMatrix();

	// Getters
	D3DXVECTOR3 GetPosition()
	{
		return m_Position;
	}
	D3DXVECTOR3 GetRotation()
	{
		return m_Rotation;
	}
	D3DXVECTOR3 GetScale()
	{
		return m_Scale;
	}
	float GetScaleX()
	{
		return m_Scale.x;
	}
	float GetScaleY()
	{
		return m_Scale.y;
	}
	float GetScaleZ()
	{
		return m_Scale.z;
	}

	D3DXMATRIXA16 GetWorldMatrix()
	{
		return m_WorldMatrix;
	}
	float GetDisplacement()
	{
		return m_DisplacementAmount;
	}


	// Setters
	void SetPosition(D3DXVECTOR3 position)
	{
		m_Position = position;
	}
	void Move(D3DXVECTOR3 movement)
	{
		m_Position += movement;
	}
	void SetRotation(D3DXVECTOR3 rotation)
	{
		m_Rotation = rotation;
	}
	void Rotate(D3DXVECTOR3 rotation)
	{
		m_Rotation += rotation;
	}
	void SetScale(D3DXVECTOR3 scale)
	{
		m_Scale = scale;
	}
	void SetScale(float scale)
	{
		m_Scale = D3DXVECTOR3(scale, scale, scale);
	}
	void SetDisplacement(float displacement)
	{
		m_DisplacementAmount = displacement;
	}
	
	void* operator new(size_t i)
	{
		return _mm_malloc(i, 16);
	}

		void operator delete(void* p)
	{
		_mm_free(p);
	}
};


#endif