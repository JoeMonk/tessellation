#ifndef __CFMOD
#define __CFMOD

#include "fmod.hpp"
#include "fmod_output.h"
#include "fmod_errors.h"
#include <vector>

class CFMod
{
private:
	FMOD_RESULT result;
	FMOD::System *fmodSystem;
	FMOD_RESULT fModResult;
	unsigned int version;
	int numdrivers;
	FMOD_SPEAKERMODE speakermode;
	FMOD_CAPS caps;
	FMOD::Sound* playSound;
	FMOD::Channel* channel;
	bool currentMute;
public:
	CFMod::CFMod();
	CFMod::~CFMod();
	FMOD::Sound* FModPlaySound(char* sound, bool repeat);
	bool Mute(bool mute);
	bool CurrentMute()
	{
		return currentMute;
	}
};

#endif