#include "QuadTessModel.h"

CQuadTessModel::CQuadTessModel(ID3DX11EffectTechnique* tessellationTechnique)
{
	m_Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_Rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	SetScale(1.0f);
	m_DisplacementAmount = 0.0f;
	UpdateMatrix();

	m_pQuadVertexBuffer = NULL;
	m_pQuadIndexBuffer = NULL;
	m_pQuadVertexLayout = NULL;

	m_GridWidth = 0.0f;
	m_GridX = 0;
	m_GridY = 0;

	m_VectorSize = 0;

	//Set the layout here, doesn't change for this class
	D3D11_INPUT_ELEMENT_DESC SceneLayout[2] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	

	D3DX11_PASS_DESC PassDesc;
	tessellationTechnique->GetPassByIndex(0)->GetDesc(&PassDesc);

	HRESULT hr = g_pd3dDevice->CreateInputLayout(SceneLayout, ARRAYSIZE(SceneLayout), PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_pQuadVertexLayout);
}

CQuadTessModel::~CQuadTessModel()
{
	if (m_pQuadVertexBuffer)		m_pQuadVertexBuffer->Release();
	if (m_pQuadIndexBuffer)			m_pQuadIndexBuffer->Release();
	if (m_pQuadVertexLayout)		m_pQuadVertexLayout->Release();
	if (m_pHeightMap)				m_pHeightMap->Release();
}

bool CQuadTessModel::LoadMaps(LPCSTR heightMap)
{
	// Create a WCHAR pointer
	const WCHAR *pwcsName;
	// Required size
	int nChars = MultiByteToWideChar(CP_ACP, 0, heightMap, -1, NULL, 0);
	// Allocate it
	pwcsName = new WCHAR[nChars];
	MultiByteToWideChar(CP_ACP, 0, heightMap, -1, (LPWSTR)pwcsName, nChars);

	// Use it
	if (FAILED(D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, pwcsName, NULL, NULL, &m_pHeightMap, NULL))) return false;

	//  Delete it
	delete[] pwcsName;

	return true;
}

bool CQuadTessModel::LoadMaps(ID3D11ShaderResourceView* heightMap)
{
	m_pHeightMap = heightMap;

	return true;
}

bool CQuadTessModel::SetMaps(ID3DX11EffectShaderResourceVariable* HeightMapVar)
{
	if (!m_pHeightMap)
	{
		return false;
	}

	HeightMapVar->SetResource(m_pHeightMap);

	return true;
}

bool CQuadTessModel::CreateModel(unsigned int gridX, unsigned int gridY, float width, float maxTessellation)
{
	m_GridWidth = width;
	m_GridX = gridX;
	m_GridY = gridY;

	m_VectorSize = m_GridX * m_GridY * 4;

	D3DXVECTOR3 *pVerts = new D3DXVECTOR3[m_VectorSize];
	unsigned int vertex = 0;
	unsigned int offset = 0;

	for (unsigned int i = 0; i < m_GridX; i++)
	{
		for (unsigned int j = 0; j < m_GridY; j++)
		{
			pVerts[vertex] = D3DXVECTOR3(i * m_GridWidth, 0.0f, (j + 1) * m_GridWidth);
			vertex++;
			pVerts[vertex] = D3DXVECTOR3((i + 1) * m_GridWidth, 0.0f, (j + 1) * m_GridWidth);
			vertex++;
			pVerts[vertex] = D3DXVECTOR3((i + 1) * m_GridWidth, 0.0f, j * m_GridWidth);
			vertex++;
			pVerts[vertex] = D3DXVECTOR3(i * m_GridWidth, 0.0f, j * m_GridWidth);
			vertex++;
		}
	}
	offset += sizeof(D3DXVECTOR3);

	//Set tex coords
	D3DXVECTOR2 *pTexCoords = new D3DXVECTOR2[m_VectorSize];
	vertex = 0;

	for (unsigned int i = 0; i < m_GridX; i++)
	{
		for (unsigned int j = 0; j < m_GridY; j++)
		{
			pTexCoords[vertex] = D3DXVECTOR2(static_cast<float>(i) / static_cast<float>(m_GridX), static_cast<float>(j + 1) / static_cast<float>(m_GridY));
			vertex++;
			pTexCoords[vertex] = D3DXVECTOR2(static_cast<float>(i + 1) / static_cast<float>(m_GridX), static_cast<float>(j + 1) / static_cast<float>(m_GridY));
			vertex++;
			pTexCoords[vertex] = D3DXVECTOR2(static_cast<float>(i + 1) / static_cast<float>(m_GridX), static_cast<float>(j) / static_cast<float>(m_GridY));
			vertex++;
			pTexCoords[vertex] = D3DXVECTOR2(static_cast<float>(i) / static_cast<float>(m_GridX), static_cast<float>(j) / static_cast<float>(m_GridY));
			vertex++;
		}
	}
	offset += sizeof(D3DXVECTOR2);

	//Decribe the buffer to put everything in
	D3D11_BUFFER_DESC vbDesc = { 0 };
	vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER; // Type of resource
	vbDesc.ByteWidth = vertex * offset;       // size in bytes
	vbDesc.CPUAccessFlags = 0;     // D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ
	vbDesc.Usage = D3D11_USAGE_DEFAULT;    // D3D11_USAGE_DEFAULT, D3D11_USAGE_IMMUTABLE, D3D11_USAGE_DYNAMIC, D3D11_USAGE_STAGING

	//Create a place and put everything in a stream
	struct info
	{
		D3DXVECTOR3 verts;
		D3DXVECTOR2 texCoords;
	};

	info *data = new info[m_VectorSize];

	for (unsigned int i = 0; i < m_VectorSize; i++)
	{
		data[i].verts = pVerts[i];
		data[i].texCoords = pTexCoords[i];
	}

	//Put the data into the buffer
	D3D11_SUBRESOURCE_DATA vbData;
	vbData.pSysMem = data;

	if (FAILED(g_pd3dDevice->CreateBuffer(&vbDesc, &vbData, &m_pQuadVertexBuffer))) return false;

	//Delete leftovers
	delete[] (pVerts);
	delete[] (pTexCoords);
	delete[] (data);

	//Set indices
	std::vector<unsigned int> indicesQuad;
	indicesQuad.resize(vertex);

	for (unsigned int i = 0; i < vertex; i++)
	{
		indicesQuad[i] = i;
	}


	vbDesc = { 0 };
	vbDesc.BindFlags = D3D11_BIND_INDEX_BUFFER; // Type of resource
	vbDesc.ByteWidth = vertex * sizeof(unsigned int);       // size in bytes
	vbDesc.CPUAccessFlags = 0;     // D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ
	vbDesc.Usage = D3D11_USAGE_DEFAULT;    // D3D11_USAGE_DEFAULT, D3D11_USAGE_IMMUTABLE, D3D11_USAGE_DYNAMIC, D3D11_USAGE_STAGING

	vbData = { 0 };
	vbData.pSysMem = (void *)& indicesQuad[0];

	if (FAILED(g_pd3dDevice->CreateBuffer(&vbDesc, &vbData, &m_pQuadIndexBuffer))) return false;

	return true;
}

bool CQuadTessModel::Render(ID3DX11EffectTechnique* tessellationTechnique)
{
	UINT Stride = 20;
	UINT Offset = 0;

	D3DX11_TECHNIQUE_DESC techDesc;
	tessellationTechnique->GetDesc(&techDesc);

	g_pd3dContext->IASetInputLayout(m_pQuadVertexLayout);
	g_pd3dContext->IASetVertexBuffers(0, 1, &m_pQuadVertexBuffer, &Stride, &Offset);
	g_pd3dContext->IASetIndexBuffer(m_pQuadIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	tessellationTechnique->GetPassByIndex(0)->Apply(0, g_pd3dContext);
	g_pd3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);
	g_pd3dContext->DrawIndexed(m_GridX * m_GridY * 4, 0, 0);

	return true;
}

void CQuadTessModel::UpdateMatrix()
{
	// Make a matrix for position and scaling, and one each for X, Y & Z rotations
	D3DXMATRIX matrixXRot, matrixYRot, matrixZRot, matrixTranslation, matrixScaling;
	D3DXMatrixRotationX(&matrixXRot, m_Rotation.x);
	D3DXMatrixRotationY(&matrixYRot, m_Rotation.y);
	D3DXMatrixRotationZ(&matrixZRot, m_Rotation.z);
	D3DXMatrixTranslation(&matrixTranslation, m_Position.x, m_Position.y, m_Position.z);
	D3DXMatrixScaling(&matrixScaling, m_Scale.x, m_Scale.y, m_Scale.z);

	// Multiply above matrices together to get the effect of them all combined - this makes the world matrix for the rendering pipeline
	// Order of multiplication is important, get slightly different control mechanism depending on order
	m_WorldMatrix = matrixScaling * matrixZRot * matrixXRot * matrixYRot * matrixTranslation;
}