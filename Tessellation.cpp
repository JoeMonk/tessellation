//--------------------------------------------------------------------------------------
//	Tessellation.cpp
//
//	DirectX 11 Tessellation
//--------------------------------------------------------------------------------------

#include <string>
#include <sstream>
#include <list>
#include <fstream>
#include <random>
#include <time.h>
using namespace std;

// General definitions used across all the project source files
#include "Defines.h"

// Declarations for supporting source files
#include "Model.h" 
#include "Camera.h"
#include "CTimer.h"
#include "Input.h"

#include "Resource.h" // Resource file (used to add icon for application)
#include <assert.h>

#include "AntTweakBar.h"
#include "FW1FontWrapper.h"

#include "CFMod.h"

#include "QuadTessModel.h"
#include "HeightMapModel.h"


//--------------------------------------------------------------------------------------
// Scene Data
//--------------------------------------------------------------------------------------

// Models and cameras encapsulated in simple classes
CModel* Stars;
CModel* Cube;
CCamera* MainCamera;

// Textures
ID3D11ShaderResourceView* StarsDiffuseMap = NULL;
ID3D11ShaderResourceView* LightDiffuseMap = NULL;
ID3D11ShaderResourceView* GroundDiffuseMap = NULL;
ID3D11ShaderResourceView* GroundNormalHeightMap = NULL;
ID3D11ShaderResourceView* NoiseMap = NULL;
ID3DX11EffectShaderResourceVariable* NoiseMapVar = NULL;
ID3D11ShaderResourceView* WaterMap = NULL;
ID3DX11EffectShaderResourceVariable* WaterMapVar = NULL;
ID3D11ShaderResourceView* RockMap = NULL;
ID3DX11EffectShaderResourceVariable* RockMapVar = NULL;
ID3D11ShaderResourceView* GroundMap = NULL;
ID3DX11EffectShaderResourceVariable* GroundMapVar = NULL;
ID3D11ShaderResourceView* SnowMap = NULL;
ID3DX11EffectShaderResourceVariable* SnowMapVar = NULL;


// Light data stored manually, a light class would be helpful - but it's an assignment task for the second years
D3DXVECTOR4 BackgroundColour = D3DXVECTOR4( 0.3f, 0.3f, 0.4f, 1.0f );
D3DXVECTOR3 AmbientColour    = D3DXVECTOR3( 0.4f, 0.4f, 0.5f );
D3DXVECTOR3 Light1Colour     = D3DXVECTOR3( 0.8f, 0.8f, 1.0f ) * 128;
D3DXVECTOR3 Light2Colour     = D3DXVECTOR3( 1.0f, 0.8f, 0.2f ) * 0;

// Display models where the lights are. One of the lights will follow an orbit
CModel* Light1;
CModel* Light2;
float LightOrbitRadius = 3584.0f;
float LightOrbitSpeed  = 0.6f;
float SpecularPower = 4.0f;
float WaterOffset = 0.0f;

//**** Toggle wireframe for the tessellated model
bool TessWireframe = false;


//--------------------------------------------------------------------------------------
// Shader Variables
//--------------------------------------------------------------------------------------
// Variables to connect C++ code to HLSL shaders

// Effects / techniques
ID3DX11Effect*          Effect = NULL;
ID3DX11EffectTechnique* PixelLitTexTechnique = NULL;
ID3DX11EffectTechnique* AdditiveTexTintTechnique = NULL;
ID3DX11EffectTechnique* TessellationTechnique = NULL;
ID3DX11EffectTechnique* TessellationWireTechnique = NULL;

// Matrices
ID3DX11EffectMatrixVariable* WorldMatrixVar = NULL;
ID3DX11EffectMatrixVariable* ViewMatrixVar = NULL;
ID3DX11EffectMatrixVariable* InvViewMatrixVar = NULL;
ID3DX11EffectMatrixVariable* ProjMatrixVar = NULL;
ID3DX11EffectMatrixVariable* ViewProjMatrixVar = NULL;

// Dimensions of the viewport
ID3DX11EffectScalarVariable* ViewportWidthVar = NULL; 
ID3DX11EffectScalarVariable* ViewportHeightVar = NULL;

// Textures
ID3DX11EffectShaderResourceVariable* DiffuseMapVar = NULL;
ID3DX11EffectShaderResourceVariable* NormalHeightMapVar = NULL;

// Light Effect variables
ID3DX11EffectVectorVariable* CameraPosVar = NULL;
ID3DX11EffectVectorVariable* Light1PosVar = NULL;
ID3DX11EffectVectorVariable* Light1ColourVar = NULL;
ID3DX11EffectVectorVariable* Light2PosVar = NULL;
ID3DX11EffectVectorVariable* Light2ColourVar = NULL;
ID3DX11EffectVectorVariable* AmbientColourVar = NULL;
ID3DX11EffectScalarVariable* SpecularPowerVar = NULL;
ID3DX11EffectScalarVariable* WaterOffsetVar = NULL;

// Displacement mapping variables
ID3DX11EffectScalarVariable* DisplacementScaleVar = NULL;
ID3DX11EffectScalarVariable* MinTessDistanceVar = NULL;
ID3DX11EffectScalarVariable* MaxTessDistanceVar = NULL;
ID3DX11EffectScalarVariable* MinTessellationVar = NULL;
ID3DX11EffectScalarVariable* MaxTessellationVar = NULL;
ID3DX11EffectScalarVariable* NoiseScaleVar = NULL;
ID3DX11EffectScalarVariable* NormalModifierVar = NULL;

// Other 
ID3DX11EffectVectorVariable* TintColourVar = NULL;

//--------------------------------------------------------------------------------------
// DirectX Variables
//--------------------------------------------------------------------------------------

// The main D3D interface
ID3D11Device*        g_pd3dDevice = NULL;  // The main device pointer has been split into two, one for the graphics device itself...
ID3D11DeviceContext* g_pd3dContext = NULL; // ...and one pointer for the current rendering thread of execution - allows multithreaded rendering

// Variables used to setup D3D
IDXGISwapChain*           SwapChain = NULL;
ID3D11Texture2D*          DepthStencil = NULL;
ID3D11DepthStencilView*   DepthStencilView = NULL;
ID3D11ShaderResourceView* DepthShaderView;
ID3D11RenderTargetView*   BackBufferRenderTarget = NULL;

// Variables used to setup the Window
HINSTANCE HInst = NULL;
HWND      HWnd = NULL;
int       g_ViewportWidth;
int       g_ViewportHeight;


ID3D11Buffer*        g_pQuadVertexBuffer = NULL;
ID3D11Buffer*        g_pQuadIndexBuffer = NULL;
ID3D11InputLayout*   g_pQuadVertexLayout = NULL;
const float g_GridWidth = 32.0f;
const int g_GridX = 128;
const int g_GridY = 128;
const float g_Scale = 8.0f;

std::string g_HeightmapName = "Height map.dds";
float g_NoiseScale		= 4.0f;
float g_Displacement	= 4096.0f;
float g_MinTessellation = 1.0f;
float g_MaxTessellation = 16.0f;	// Set so the heights can be blurred and normals generated properly
float g_MinTessDistance = 2048.0f;
float g_MaxTessDistance = 16384.0f;

float g_LightSpeed = 2.0f;

float g_Attenuation = 24.0f;
ID3DX11EffectScalarVariable* AttenuationVar = NULL;

CQuadTessModel*     terrain = NULL;

// Height blurring stuff
ID3D11Texture2D*          BlurredHeightTexture = NULL;
ID3D11RenderTargetView*   BlurredHeightTarget = NULL;
ID3D11ShaderResourceView* BlurredHeightShaderResource = NULL;

ID3DX11EffectTechnique* HeightBlurringTechnique = NULL;
ID3DX11EffectShaderResourceVariable* BlurredHeightMapVar = NULL;

// Heightmap generation
ID3D11Texture2D*          GeneratedHeightTexture = NULL;
ID3D11RenderTargetView*   GeneratedHeightTarget = NULL;
ID3D11ShaderResourceView* GeneratedHeightShaderResource = NULL;

ID3DX11EffectTechnique* GeneratedHeightTechnique = NULL;
ID3DX11EffectShaderResourceVariable* GeneratedHeightMapVar = NULL;
CHeightMapModel*		GeneratedHeightMap = NULL;
unsigned int            GeneratedGridSize = 42;
float					GeneratedTessAmount = 64.0f;

//Ant Tweak Bar
TwBar* AntTweakBar;

float g_CameraMoveSpeed = 1024.0f;
float g_CameraRotSpeed = 1.8f;

// FPS/Update time stuff
IFW1Factory* g_pFW1Factory;
IFW1FontWrapper* g_pFontWrapper;
float SumUpdateTimes = 0;
int NumUpdateTimes = 0;
static const float UpdateTimePeriod = 0.5f;
float AverageUpdateTime;

CFMod* FModSound = NULL;
bool soundMute = false;

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------

bool InitDevice();
void ReleaseResources();
bool LoadEffectFile();
bool InitScene();
void UpdateScene( float frameTime );
void RenderModels();
void RenderScene();
bool InitWindow( HINSTANCE hInstance, int nCmdShow );
LRESULT CALLBACK WndProc( HWND, UINT, WPARAM, LPARAM );

void CreateBlurredHeightmap();
void GenerateHeightmap();

void TW_CALL CreateRandomHeightmap(void *clientData)
{
	if (GeneratedHeightMap)
	{
		delete(GeneratedHeightMap);
		GeneratedHeightMap = NULL;
	}
	GeneratedHeightMap = new CHeightMapModel(GeneratedHeightTechnique);
	GeneratedHeightMap->CreateModel(GeneratedGridSize, GeneratedGridSize, g_GridWidth, GeneratedTessAmount);
	GeneratedHeightMap->LoadMaps(("Media/" + g_HeightmapName).c_str());

	GenerateHeightmap();

	terrain->LoadMaps(GeneratedHeightShaderResource);

	CreateBlurredHeightmap();
}
void TW_CALL SaveGeneratedHeightmap(void *clientData)
{
	HRESULT hr = D3DX11SaveTextureToFile(g_pd3dContext, GeneratedHeightTexture, D3DX11_IFF_DDS, L".\\GeneratedHeightmap.dds");
}

void TW_CALL LoadBlurredHeightmap(void *clientData)
{
	//Set stuff here for updating only
	if (!(terrain->LoadMaps(("Media/" + g_HeightmapName).c_str())))
	{
		terrain->LoadMaps(("Media/" + g_HeightmapName).c_str());
	}
	CreateBlurredHeightmap();
}

void TW_CALL UpdateHeightmap(void *clientData)
{
	CreateBlurredHeightmap();
}

void TW_CALL SaveBlurredHeightmap(void *clientData)
{
	HRESULT hr = D3DX11SaveTextureToFile(g_pd3dContext, BlurredHeightTexture, D3DX11_IFF_DDS, L".\\Heightmap.dds");
}
void TW_CALL CopyStdStringToClient(std::string& destinationClientString, const std::string& sourceLibraryString)
{
	destinationClientString = sourceLibraryString;
}

//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
bool InitDevice()
{
	HRESULT hr = S_OK;


	////////////////////////////////
	// Initialise Direct3D

	// Calculate the visible area the window we are using - the "client rectangle" refered to in the first function is the 
	// size of the interior of the window, i.e. excluding the frame and title
	RECT rc;
	GetClientRect( HWnd, &rc );
	g_ViewportWidth = rc.right - rc.left;
	g_ViewportHeight = rc.bottom - rc.top;


	// Create a Direct3D device and create a swap-chain (create a back buffer to render to)
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory( &sd, sizeof( sd ) );
	sd.BufferCount = 1;
	sd.BufferDesc.Width = g_ViewportWidth;             // Target window size
	sd.BufferDesc.Height = g_ViewportHeight;           // --"--
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // Pixel format of target window
	sd.BufferDesc.RefreshRate.Numerator = 60;          // Refresh rate of monitor
	sd.BufferDesc.RefreshRate.Denominator = 1;         // --"--
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.OutputWindow = HWnd;                            // Target window
	sd.Windowed = TRUE;                                // Whether to render in a window (TRUE) or go fullscreen (FALSE)
	hr = D3D11CreateDeviceAndSwapChain( NULL, D3D_DRIVER_TYPE_HARDWARE, 0, 0, 0, 0, D3D11_SDK_VERSION, &sd, &SwapChain, &g_pd3dDevice, NULL, &g_pd3dContext ); //D3D11_CREATE_DEVICE_DEBUG
	if( FAILED( hr ) ) return false;


	// Indicate that the back-buffer can be "viewed" as a render target - standard behaviour
	ID3D11Texture2D* pBackBuffer;
	hr = SwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer );
	if( FAILED( hr ) ) return false;
	hr = g_pd3dDevice->CreateRenderTargetView( pBackBuffer, NULL, &BackBufferRenderTarget );
	pBackBuffer->Release();
	if( FAILED( hr ) ) return false;


	// Create a texture for a depth buffer
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = g_ViewportWidth;
	descDepth.Height = g_ViewportHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R32_TYPELESS;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = g_pd3dDevice->CreateTexture2D( &descDepth, NULL, &DepthStencil );
	if( FAILED( hr ) ) return false;

	// Create the depth stencil view, i.e. indicate that the texture just created is to be used as a depth buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	descDSV.Format = DXGI_FORMAT_D32_FLOAT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Flags = 0;
	descDSV.Texture2D.MipSlice = 0;
	hr = g_pd3dDevice->CreateDepthStencilView( DepthStencil, &descDSV, &DepthStencilView );
	if( FAILED( hr ) ) return false;

	srand(static_cast<unsigned int>(time(NULL)));

	//Create a font wrapper
	hr = FW1CreateFactory(FW1_VERSION, &g_pFW1Factory);

	if (FAILED(hr)) return false;

	hr = g_pFW1Factory->CreateFontWrapper(g_pd3dDevice, L"Arial", &g_pFontWrapper);

	if (FAILED(hr)) return false;

	//Init the ant tweak bar
	TwInit(TW_DIRECT3D11, g_pd3dDevice);

	TwCopyStdStringToClientFunc(CopyStdStringToClient);

	AntTweakBar = TwNewBar("TweakBar");
	int barSize[2] = { 280, 480 };
	TwSetParam(AntTweakBar, NULL, "size", TW_PARAM_INT32, 2, barSize);

	TwAddSeparator(AntTweakBar, "Sound Options", NULL);

	TwAddVarRW(AntTweakBar, "Mute", TW_TYPE_BOOLCPP, &soundMute, " label='Mute sound' ");

	//Add things to the bar
	TwAddSeparator(AntTweakBar, "Tessellation Options", NULL);

	TwAddVarRW(AntTweakBar, "Wireframe", TW_TYPE_BOOLCPP, &TessWireframe, NULL);
	TwDefine("TweakBar/Wireframe key=0");

	TwAddVarRW(AntTweakBar, "Min_Tessellation", TW_TYPE_FLOAT, &g_MinTessellation, NULL);
	TwDefine("TweakBar/Min_Tessellation min=1 max=64 step=1");

	TwAddVarRW(AntTweakBar, "Max_Tessellation", TW_TYPE_FLOAT, &g_MaxTessellation, NULL);
	TwDefine("TweakBar/Max_Tessellation min=1 max=64 step=1");

	TwAddVarRW(AntTweakBar, "Min_Tess_Distance", TW_TYPE_FLOAT, &g_MinTessDistance, " label='Min Tessellation Distance' ");
	TwDefine("TweakBar/Min_Tess_Distance min=0 max=100000 step=256");
	
	TwAddVarRW(AntTweakBar, "Max_Tess_Distance", TW_TYPE_FLOAT, &g_MaxTessDistance, " label='Max Tessellation Distance' ");
	TwDefine("TweakBar/Max_Tess_Distance min=0 max=100000 step=256");
	
	TwAddVarRW(AntTweakBar, "Noise_Scale", TW_TYPE_FLOAT, &g_NoiseScale, NULL);
	TwDefine("TweakBar/Noise_Scale min=0 max=256 step=2");


	TwAddSeparator(AntTweakBar, "Heightmap Generation", NULL);

	TwAddVarRW(AntTweakBar, "Grid_size", TW_TYPE_INT32, &GeneratedGridSize, NULL);
	TwDefine("TweakBar/Grid_size min=4 max=256 step=2 label='Grid Size' ");

	TwAddVarRW(AntTweakBar, "Tess_amount", TW_TYPE_FLOAT, &GeneratedTessAmount, NULL);
	TwDefine("TweakBar/Tess_amount min=1 max=64 step=2 label='Tessellation Amount' ");

	TwAddButton(AntTweakBar, "Random_heightmap", CreateRandomHeightmap, NULL, " label='Random Heightmap' ");

	TwAddButton(AntTweakBar, "Save_rand_heightmap", SaveGeneratedHeightmap, NULL, " label='Save Random Heightmap' ");
	

	TwAddSeparator(AntTweakBar, "Heightmap Options", NULL);
	
	TwAddVarRW(AntTweakBar, "Displacement", TW_TYPE_FLOAT, &g_Displacement, NULL);
	TwDefine("TweakBar/Displacement min=0 max=8192 step=512");

	TwAddButton(AntTweakBar, "Update_heightmap", UpdateHeightmap, NULL, " label='Update Heightmap' ");

	TwAddButton(AntTweakBar, "Save_heightmap", SaveBlurredHeightmap, NULL, " label='Save Heightmap' ");

	TwAddVarRW(AntTweakBar, "Change_heightmap", TW_TYPE_STDSTRING, &g_HeightmapName, " label='Change Heightmap' ");

	TwAddButton(AntTweakBar, "Load_heightmap", LoadBlurredHeightmap, NULL, " label='Load Heightmap' ");


	TwAddSeparator(AntTweakBar, "Camera Options", NULL);

	TwAddVarRW(AntTweakBar, "Camera_Move_Speed", TW_TYPE_FLOAT, &g_CameraMoveSpeed, NULL);
	TwDefine("TweakBar/Camera_Move_Speed min=0 max=4096 step=10");

	TwAddVarRW(AntTweakBar, "Camera_Rot_Speed", TW_TYPE_FLOAT, &g_CameraRotSpeed, NULL);
	TwDefine("TweakBar/Camera_Rot_Speed min=0 max=8 step=0.1");


	TwAddSeparator(AntTweakBar, "Light Options", NULL);

	TwAddVarRW(AntTweakBar, "Light1_Move_Speed", TW_TYPE_FLOAT, &g_LightSpeed, NULL);
	TwDefine("TweakBar/Light1_Move_Speed min=0 max=100 step=1 label='Light Control Speed'");

	TwAddVarRW(AntTweakBar, "Light2_Move_Speed", TW_TYPE_FLOAT, &LightOrbitSpeed, NULL);
	TwDefine("TweakBar/Light2_Move_Speed min=0 max=8.0 step=0.1");

	TwAddVarRW(AntTweakBar, "Light2_Move_Radius", TW_TYPE_FLOAT, &LightOrbitRadius, NULL);
	TwDefine("TweakBar/Light2_Move_Radius min=0 max=4096 step=16");

	TwAddVarRW(AntTweakBar, "Light_Attenuation", TW_TYPE_FLOAT, &g_Attenuation, NULL);
	TwDefine("TweakBar/Light_Attenuation min=0 max=128 step=0.5");

	FModSound = new CFMod();

	return true;
}


// Release the memory held by all objects created
void ReleaseResources()
{
	if( g_pd3dContext ) g_pd3dContext->ClearState();

	TwTerminate();

	if (g_pFontWrapper)			g_pFontWrapper->Release();
	if (g_pFW1Factory)			g_pFW1Factory->Release();
	if (FModSound)				delete (FModSound);

	delete Light2;
	delete Light1;
	delete Stars;
	delete MainCamera;
	delete terrain;

	if (GroundDiffuseMap)				GroundDiffuseMap->Release();
	if (GroundNormalHeightMap)			GroundNormalHeightMap->Release();
    if (LightDiffuseMap)				LightDiffuseMap->Release();
    if (StarsDiffuseMap)				StarsDiffuseMap->Release();
	if (NoiseMap)						NoiseMap->Release();
	if (WaterMap)						WaterMap->Release();
	if (RockMap)						RockMap->Release();
	if (GroundMap)						GroundMap->Release();
	if (SnowMap)						SnowMap->Release();
	if (Effect)							Effect->Release();
	if (BlurredHeightShaderResource)	BlurredHeightShaderResource->Release();
	if (BlurredHeightTarget)			BlurredHeightTarget->Release();
	if (BlurredHeightTexture)			BlurredHeightTexture->Release();
	if (DepthShaderView)				DepthShaderView->Release();
	if (DepthStencilView)				DepthStencilView->Release();
	if (BackBufferRenderTarget)			BackBufferRenderTarget->Release();
	if (DepthStencil)					DepthStencil->Release();
	if (SwapChain)						SwapChain->Release();
	if (g_pd3dDevice)					g_pd3dDevice->Release();
}



//--------------------------------------------------------------------------------------
// Load and compile Effect file (.fx file containing shaders)
//--------------------------------------------------------------------------------------

// All techniques in one file in this lab
bool LoadEffectFile()
{

	ID3DBlob* pCompiled; // This strangely typed variable collects the compiled shader code (not ready for use as an effect yet, see next code)
	ID3DBlob* pErrors;   // Variable to collect error messages#

#ifndef _DEBUG
	DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS; // These "flags" are used to set the compiler options
#else
	DWORD dwShaderFlags = D3D10_SHADER_DEBUG | D3D10_SHADER_ENABLE_STRICTNESS | D3D10_SHADER_WARNINGS_ARE_ERRORS;
#endif

	HRESULT hr = D3DX11CompileFromFile(L"Tessellation.fx", NULL, NULL, NULL, "fx_5_0", dwShaderFlags, 0, NULL, &pCompiled, &pErrors, NULL);
	if( FAILED( hr ) )
	{
		if (pErrors != 0)  MessageBox( NULL, CA2CT(reinterpret_cast<char*>(pErrors->GetBufferPointer())), L"Error", MB_OK ); // Compiler error: display error message
		else               MessageBox( NULL, L"Error loading FX file. Ensure your FX file is in the same folder as this executable.", L"Error", MB_OK );  // No error message - probably file not found
		return false;
	}


	// Load and compile the effect file
	hr = D3DX11CreateEffectFromMemory( pCompiled->GetBufferPointer(), pCompiled->GetBufferSize(), 0, g_pd3dDevice, &Effect );
	if( FAILED( hr ) )
	{
		MessageBox( NULL, L"Error creating effects", L"Error", MB_OK );
		return false;
	}

	// Select techniques from the compiled effect file
	PixelLitTexTechnique      = Effect->GetTechniqueByName( "PixelLitTex" );
	AdditiveTexTintTechnique  = Effect->GetTechniqueByName( "AdditiveTexTint" );
	TessellationTechnique     = Effect->GetTechniqueByName( "DisplacementMapping" );
	TessellationWireTechnique = Effect->GetTechniqueByName( "DisplacementMappingWireframe" );
	HeightBlurringTechnique = Effect->GetTechniqueByName("HeightBlurring");
	GeneratedHeightTechnique  = Effect->GetTechniqueByName("HeightGeneration");

	// Create variables to access global variables in the shaders from C++
	WorldMatrixVar      = Effect->GetVariableByName( "WorldMatrix"    )->AsMatrix();
	ViewMatrixVar       = Effect->GetVariableByName( "ViewMatrix"     )->AsMatrix();
	InvViewMatrixVar    = Effect->GetVariableByName( "InvViewMatrix"  )->AsMatrix();
	ProjMatrixVar       = Effect->GetVariableByName( "ProjMatrix"     )->AsMatrix();
	ViewProjMatrixVar   = Effect->GetVariableByName( "ViewProjMatrix" )->AsMatrix();
	

	// Textures in shader (shader resources)
	DiffuseMapVar       = Effect->GetVariableByName( "DiffuseMap" )->AsShaderResource();
	NormalHeightMapVar  = Effect->GetVariableByName( "NormalHeightMap" )->AsShaderResource();
	BlurredHeightMapVar = Effect->GetVariableByName("GeneratedHeightMap")->AsShaderResource();
	NoiseMapVar			= Effect->GetVariableByName("NoiseMap")->AsShaderResource();
	WaterMapVar         = Effect->GetVariableByName("WaterMap")->AsShaderResource();
	RockMapVar			= Effect->GetVariableByName("RockMap")->AsShaderResource();
	GroundMapVar		= Effect->GetVariableByName("GroundMap")->AsShaderResource();
	SnowMapVar			= Effect->GetVariableByName("SnowMap")->AsShaderResource();

	// Viewport dimensions
	ViewportWidthVar  = Effect->GetVariableByName( "ViewportWidth"  )->AsScalar();
	ViewportHeightVar = Effect->GetVariableByName( "ViewportHeight" )->AsScalar();

	// Also access shader variables needed for lighting
	CameraPosVar     = Effect->GetVariableByName( "CameraPos"     )->AsVector();
	Light1PosVar     = Effect->GetVariableByName( "Light1Pos"     )->AsVector();
	Light1ColourVar  = Effect->GetVariableByName( "Light1Colour"  )->AsVector();
	Light2PosVar     = Effect->GetVariableByName( "Light2Pos"     )->AsVector();
	Light2ColourVar  = Effect->GetVariableByName( "Light2Colour"  )->AsVector();
	AmbientColourVar = Effect->GetVariableByName( "AmbientColour" )->AsVector();
	SpecularPowerVar = Effect->GetVariableByName( "SpecularPower" )->AsScalar();
	WaterOffsetVar   = Effect->GetVariableByName( "WaterOffset"   )->AsScalar();
	NoiseScaleVar    = Effect->GetVariableByName( "NoiseScale"    )->AsScalar();
	AttenuationVar   = Effect->GetVariableByName( "Attenuation"   )->AsScalar();
	NormalModifierVar = Effect->GetVariableByName("NormalModifier")->AsScalar();

	// Displacement mapping
	DisplacementScaleVar = Effect->GetVariableByName("DisplacementScale")->AsScalar();
	MinTessDistanceVar   = Effect->GetVariableByName( "MinTessDistance"   )->AsScalar();
	MaxTessDistanceVar   = Effect->GetVariableByName( "MaxTessDistance"   )->AsScalar();
	MinTessellationVar   = Effect->GetVariableByName( "MinTessellation"   )->AsScalar();
	MaxTessellationVar   = Effect->GetVariableByName( "MaxTessellation"   )->AsScalar();

	// Other shader variables
	TintColourVar = Effect->GetVariableByName( "TintColour"  )->AsVector();

	return true;
}



//--------------------------------------------------------------------------------------
// Scene Setup / Update
//--------------------------------------------------------------------------------------

// Create / load the camera, models and textures for the scene
bool InitScene()
{
	///////////////////
	// Create cameras

	MainCamera = new CCamera();
	MainCamera->SetPosition(D3DXVECTOR3(0, g_Displacement, 0));
	MainCamera->SetRotation( D3DXVECTOR3(ToRadians(25.0f), ToRadians(0.0f), 0.0f) ); // ToRadians is a new helper function to convert degrees to radians

	// Create models
	Stars  = new CModel;
	Light1 = new CModel;
	Light2 = new CModel;

	// Load .X files for each model
	if (!Stars-> Load( "Media/Stars.x", PixelLitTexTechnique )) return false;
	if (!Light1->Load( "Media/Light.x", AdditiveTexTintTechnique )) return false;
	if (!Light2->Load( "Media/Light.x", AdditiveTexTintTechnique )) return false;

	// Initial positions
	Stars-> SetScale( 100000.0f );
	Light1->SetPosition(D3DXVECTOR3(30, g_Displacement + 512.0f, 0));
	Light1->SetScale( 4.0f );	
	Light2->SetPosition(D3DXVECTOR3(0, g_Displacement + 512.0f, 0));
	Light2->SetScale( 8.0f );

	//////////////////
	// Load textures

	if (FAILED(D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"Media/Flare.jpg", NULL, NULL, &LightDiffuseMap, NULL))) return false;
	if (FAILED(D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"Media/Clouds.jpg", NULL, NULL, &StarsDiffuseMap, NULL))) return false;
	if (FAILED(D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"Media/perlin_noise.png", NULL, NULL, &NoiseMap, NULL))) return false;
	if (FAILED(D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"Media/Azure Waters.jpg", NULL, NULL, &WaterMap, NULL))) return false;
	if (FAILED(D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"Media/Age of the Canyon.jpg", NULL, NULL, &RockMap, NULL))) return false;
	if (FAILED(D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"Media/Common Ground.jpg", NULL, NULL, &GroundMap, NULL))) return false;
	if (FAILED(D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"Media/Scuffed Snow.jpg", NULL, NULL, &SnowMap, NULL))) return false;


	//Make Class
	terrain = new CQuadTessModel(TessellationTechnique);
	
	terrain->CreateModel(g_GridX, g_GridY, g_GridWidth, g_MaxTessellation);

	terrain->SetScale(g_Scale);
	terrain->SetDisplacement(g_Displacement);
	terrain->SetPosition(D3DXVECTOR3(-g_GridX * g_GridWidth * terrain->GetScaleX() * 0.5f, 0.0f, -g_GridY * g_GridWidth * terrain->GetScaleY() * 0.5f));

	NoiseMapVar->SetResource(NoiseMap);
	WaterMapVar->SetResource(WaterMap);
	RockMapVar->SetResource(RockMap);
	GroundMapVar->SetResource(GroundMap);
	SnowMapVar->SetResource(SnowMap);


	terrain->LoadMaps(("Media/" + g_HeightmapName).c_str());
	CreateBlurredHeightmap();

	// http://www.looperman.com/loops/detail/53590
	FModSound->FModPlaySound("Media/Sound/Guitars Unlimited - My passion.wav", true);


	GeneratedHeightMap = new CHeightMapModel(GeneratedHeightTechnique);
	GeneratedHeightMap->CreateModel(GeneratedGridSize, GeneratedGridSize, g_GridWidth, GeneratedTessAmount);
	GeneratedHeightMap->LoadMaps(("Media/" + g_HeightmapName).c_str());

	GenerateHeightmap();

	return true;
}


//--------------------------------------------------------------------------------------
// Scene Update
//--------------------------------------------------------------------------------------

// Update the scene - move/rotate each model and the camera, then update their matrices
void UpdateScene( float frameTime )
{
	SumUpdateTimes += frameTime;
	++NumUpdateTimes;
	if (SumUpdateTimes >= UpdateTimePeriod)
	{
		AverageUpdateTime = SumUpdateTimes / NumUpdateTimes;
		SumUpdateTimes = 0.0f;
		NumUpdateTimes = 0;
	}

	// Control camera position and update its matrices (monoscopic version)
	MainCamera->Control( Key_Up, Key_Down, Key_Left, Key_Right, Key_W, Key_S, Key_A, Key_D, g_CameraMoveSpeed * frameTime, g_CameraRotSpeed * frameTime );
	MainCamera->UpdateMatrices();
	MainCamera->SetFarClip(200000.0f);
	
	Light2->Control(frameTime * 2.0f * g_LightSpeed, Key_U, Key_O, Key_L, Key_J, Key_Pause, Key_Pause, Key_Pause, Key_Pause);
	Light2->Control(frameTime * 0.1f * g_LightSpeed, Key_Pause, Key_Pause, Key_Pause, Key_Pause, Key_Pause, Key_Pause, Key_I, Key_K);

	// Update the orbiting light - a bit of a cheat with the static variable
	static float Rotate = 0.0f;
	Light1->SetPosition(Light2->GetPosition() + D3DXVECTOR3(cos(Rotate)*LightOrbitRadius, 0.0f, sin(Rotate)*LightOrbitRadius));
	Light1->UpdateMatrix();
	Rotate += LightOrbitSpeed * frameTime;

	// Objects that don't move still need a world matrix - could do this in SceneSetup function (which occurs once at the start of the app)
	Stars->UpdateMatrix();
	Light2->UpdateMatrix();

	terrain->UpdateMatrix();

	WaterOffset += frameTime;
	if (WaterOffset > 1.0f)
	{
		WaterOffset -= 1.0f;
	}

	if (soundMute != FModSound->CurrentMute())
	{
		FModSound->Mute(soundMute);
	}
}


//--------------------------------------------------------------------------------------
// Scene Rendering
//--------------------------------------------------------------------------------------

// Render all models
void RenderModels()
{
	// Choose tessellation technique with wireframe if required
	ID3DX11EffectTechnique* Tessellation = TessWireframe ? TessellationWireTechnique : TessellationTechnique;


	//Set variables
	DisplacementScaleVar->SetFloat(terrain->GetDisplacement()); // Depth of displacement
	MinTessellationVar->SetFloat(g_MinTessellation);  // Minimum amount of tessellation (1 to 64)
	MaxTessellationVar->SetFloat(g_MaxTessellation);  // Maximum --"--
	MinTessDistanceVar->SetFloat(g_MinTessDistance);  // Distance at which we get maximum tessellation (the world is about 1000 across)
	MaxTessDistanceVar->SetFloat(g_MaxTessDistance);  // --"-- minimum
	NoiseScaleVar->SetFloat(g_NoiseScale);	//The scale of the noise map
	AttenuationVar->SetFloat(g_Attenuation);
	BlurredHeightMapVar->SetResource(BlurredHeightShaderResource);

	//Render models
	WorldMatrixVar->SetMatrix((float*)terrain->GetWorldMatrix());
	terrain->Render(Tessellation);

	WorldMatrixVar->SetMatrix( (float*)Stars->GetWorldMatrix() );
    DiffuseMapVar->SetResource( StarsDiffuseMap );
	Stars->Render( PixelLitTexTechnique );

	WorldMatrixVar->SetMatrix( (float*)Light1->GetWorldMatrix() );
    DiffuseMapVar->SetResource( LightDiffuseMap );
	TintColourVar->SetRawValue( Light1Colour, 0, 12 ); // Using special shader that tints the light model to match the light colour
	Light1->Render( AdditiveTexTintTechnique );

	WorldMatrixVar->SetMatrix( (float*)Light2->GetWorldMatrix() );
    DiffuseMapVar->SetResource( LightDiffuseMap );
	TintColourVar->SetRawValue( Light2Colour, 0, 12 );
	Light2->Render( AdditiveTexTintTechnique );
}

void RenderText(const string& text, float x, float y, float fontSize,
	UINT32 colour = 0xffffffff/* ABGR format*/)
{
	std::wstring widestr = std::wstring(text.begin(), text.end());
	g_pFontWrapper->DrawString(g_pd3dContext,
		std::wstring(text.begin(), text.end()).c_str(),	//Fiddley conversion from string to wide characters
		fontSize,	// Font size
		x,			// X position
		y,			// Y position
		colour,		// Text colour, 0xAaBbGgRr
		FW1_RIGHT);
}

void RenderSceneText()
{
	// Write FPS text string
	stringstream outText;
	if (AverageUpdateTime >= 0.0f)
	{
		outText << "Frame Time: " << AverageUpdateTime * 1000.0f << "ms" << endl << "FPS:" << 1.0f / AverageUpdateTime << endl;
		RenderText(outText.str(), static_cast<float>(g_ViewportWidth - 2), 3.0, 16.0f, 0xff000000);
		RenderText(outText.str(), static_cast<float>(g_ViewportWidth - 3), 2.0, 16.0f);
		outText.str("");
	}

}

// Render everything in the scene
void RenderScene()
{
	//---------------------------
	// Common rendering settings

	// There are some common features all models that we will be rendering, set these once only

	// Pass the camera's matrices to the vertex shader and position to the vertex shader
	ViewMatrixVar->SetMatrix( (float*)&MainCamera->GetViewMatrix() );
	InvViewMatrixVar->SetMatrix( (float*)&MainCamera->GetWorldMatrix() );
	ProjMatrixVar->SetMatrix( (float*)&MainCamera->GetProjectionMatrix() );
	ViewProjMatrixVar->SetMatrix( (float*)&MainCamera->GetViewProjectionMatrix() );
	CameraPosVar->SetRawValue( MainCamera->GetPosition(), 0, 12 );

	// Pass light information to the vertex shader - lights are the same for each model *** and every render target ***
	Light1PosVar->    SetRawValue( Light1->GetPosition(), 0, 12 );  // Send 3 floats (12 bytes) from C++ LightPos variable (x,y,z) to shader counterpart (middle parameter is unused) 
	Light1ColourVar-> SetRawValue( Light1Colour, 0, 12 );
	Light2PosVar->    SetRawValue( Light2->GetPosition(), 0, 12 );
	Light2ColourVar-> SetRawValue( Light2Colour, 0, 12 );
	AmbientColourVar->SetRawValue( AmbientColour, 0, 12 );
	SpecularPowerVar->SetFloat( SpecularPower );
	WaterOffsetVar->SetFloat(WaterOffset);

	// Setup the viewport - defines which part of the back-buffer we will render to (usually all of it)
	D3D11_VIEWPORT vp;
	vp.Width  = (FLOAT)g_ViewportWidth;
	vp.Height = (FLOAT)g_ViewportHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dContext->RSSetViewports( 1, &vp );


	//---------------------------
	// Render scene

	// Select and clear back buffer & depth buffer
	g_pd3dContext->OMSetRenderTargets( 1, &BackBufferRenderTarget, DepthStencilView );
	g_pd3dContext->ClearRenderTargetView( BackBufferRenderTarget, &BackgroundColour[0] );
	g_pd3dContext->ClearDepthStencilView( DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0 );

	// Render everything
	RenderModels();

	TwDraw();

	RenderSceneText();

	// After we've finished drawing to the off-screen back buffer, we "present" it to the front buffer (the screen)
	SwapChain->Present( 0, 0 );
}



////////////////////////////////////////////////////////////////////////////////////////
// Window Setup
////////////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow ){
	// Initialise everything in turn
	if( !InitWindow( hInstance, nCmdShow) )
	{
		return 0;
	}
	if( !InitDevice() || !LoadEffectFile() || !InitScene() )
	{
		ReleaseResources();
		return 0;
	}

	// Initialise simple input functions
	InitInput();

	// Initialise a timer class, start it counting now
	CTimer Timer;
	Timer.Start();

	// Main message loop
	MSG msg = {0};
	while( WM_QUIT != msg.message )
	{
		// Check for and deal with window messages (window resizing, minimizing, etc.). When there are none, the window is idle and D3D rendering occurs
		if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
		{
			// Deal with messages
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else // Otherwise render & update
		{
			RenderScene();

			// Get the time passed since the last frame
			float frameTime = Timer.GetLapTime();
			UpdateScene( frameTime );
			

			if (KeyHit( Key_Escape )) 
			{
				DestroyWindow( HWnd );
			}

		}
	}

	ReleaseResources();

	return ( int )msg.wParam;
}


//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
bool InitWindow( HINSTANCE hInstance, int nCmdShow )
{
	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof( WNDCLASSEX );
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon( hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
	wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
	wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"TutorialWindowClass";
	wcex.hIconSm = LoadIcon( wcex.hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
	if( !RegisterClassEx( &wcex ) )	return false;

	// Create window
	HInst = hInstance;
	RECT rc = { 0, 0, 1600, 900 };
	AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
	HWnd = CreateWindow( L"TutorialWindowClass", L"Tessellation", WS_OVERLAPPEDWINDOW,
	                     CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, NULL );
	if( !HWnd )	return false;

	ShowWindow( HWnd, nCmdShow );

	return true;
}


//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	PAINTSTRUCT ps;
	HDC hdc;

	if (TwEventWin(hWnd, message, wParam, lParam))
	{
		return 0;
	}

	switch( message )
	{
		case WM_PAINT:
			hdc = BeginPaint( hWnd, &ps );
			EndPaint( hWnd, &ps );
			break;

		case WM_DESTROY:
			PostQuitMessage( 0 );
			break;

		// These windows messages (WM_KEYXXXX) can be used to get keyboard input to the window
		// This application has added some simple functions (not DirectX) to process these messages (all in Input.cpp/h)
		case WM_KEYDOWN:
			KeyDownEvent( static_cast<EKeyState>(wParam) );
			break;

		case WM_KEYUP:
			KeyUpEvent( static_cast<EKeyState>(wParam) );
			break;
		
		default:
			return DefWindowProc( hWnd, message, wParam, lParam );
	}

	return 0;
}

void CreateBlurredHeightmap()
{
	terrain->SetDisplacement(g_Displacement);

	// Blur map creation
	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = static_cast<unsigned int>(g_GridX * g_MaxTessellation);  // Match views to viewport size
	textureDesc.Height = static_cast<unsigned int>(g_GridY * g_MaxTessellation);
	textureDesc.MipLevels = 1; // No mip-maps when rendering to textures (or we will have to render every level)
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32_FLOAT; // 32 bit float in the red channel
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE; // Indicate we will use texture as render target, and pass it to shaders
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	g_pd3dDevice->CreateTexture2D(&textureDesc, NULL, &BlurredHeightTexture);

	// Get a "view" of the texture as a render target - giving us an interface for rendering to the texture
	g_pd3dDevice->CreateRenderTargetView(BlurredHeightTexture, NULL, &BlurredHeightTarget);

	// And get a shader-resource "view" - giving us an interface for passing the texture to shaders
	D3D11_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = textureDesc.Format;
	srDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;
	g_pd3dDevice->CreateShaderResourceView(BlurredHeightTexture, &srDesc, &BlurredHeightShaderResource);

	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)g_GridX * g_MaxTessellation;
	vp.Height = (FLOAT)g_GridY * g_MaxTessellation;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dContext->RSSetViewports(1, &vp);

	//---------------------------
	// Render scene

	// Select and clear back buffer & depth buffer
	g_pd3dContext->OMSetRenderTargets(1, &BlurredHeightTarget, NULL);
	g_pd3dContext->ClearRenderTargetView(BlurredHeightTarget, &BackgroundColour[0]);

	//Set variables
	MaxTessellationVar->SetFloat(g_MaxTessellation);  // Maximum --"--
	NormalModifierVar->SetFloat((1.0f / g_MaxTessellation) * 64.0f * 4.0f);

	//Render models
	terrain->SetMaps(NormalHeightMapVar);
	terrain->Render(HeightBlurringTechnique);
}

void GenerateHeightmap()
{
	terrain->SetDisplacement(g_Displacement);

	// Blur map creation
	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = static_cast<unsigned int>(GeneratedGridSize * GeneratedTessAmount);  // Match views to viewport size
	textureDesc.Height = static_cast<unsigned int>(GeneratedGridSize * GeneratedTessAmount);
	textureDesc.MipLevels = 1; // No mip-maps when rendering to textures (or we will have to render every level)
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT; // 32 bit float in the red channel
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE; // Indicate we will use texture as render target, and pass it to shaders
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	g_pd3dDevice->CreateTexture2D(&textureDesc, NULL, &GeneratedHeightTexture);

	// Get a "view" of the texture as a render target - giving us an interface for rendering to the texture
	g_pd3dDevice->CreateRenderTargetView(GeneratedHeightTexture, NULL, &GeneratedHeightTarget);

	// And get a shader-resource "view" - giving us an interface for passing the texture to shaders
	D3D11_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = textureDesc.Format;
	srDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;
	g_pd3dDevice->CreateShaderResourceView(GeneratedHeightTexture, &srDesc, &GeneratedHeightShaderResource);

	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)GeneratedGridSize * GeneratedTessAmount;
	vp.Height = (FLOAT)GeneratedGridSize * GeneratedTessAmount;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dContext->RSSetViewports(1, &vp);

	//---------------------------
	// Render scene

	// Select and clear back buffer & depth buffer
	D3DXVECTOR4 colour = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
	g_pd3dContext->OMSetRenderTargets(1, &GeneratedHeightTarget, NULL);
	g_pd3dContext->ClearRenderTargetView(GeneratedHeightTarget, &colour[0]);

	//Set variables
	MaxTessellationVar->SetFloat(GeneratedTessAmount);  // Maximum --"--

	//Render models
	GeneratedHeightMap->SetMaps(NormalHeightMapVar);
	GeneratedHeightMap->Render(GeneratedHeightTechnique);
}