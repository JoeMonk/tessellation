#include "CFMod.h"

CFMod::CFMod()
{
	result = FMOD::System_Create(&fmodSystem);
	ERRCHECK(result);
	result = fmodSystem->getVersion(&version);
	ERRCHECK(result);
	result = fmodSystem->getNumDrivers(&numdrivers);
	ERRCHECK(result);
	if (numdrivers == 0)
	{
		result = fmodSystem->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
		ERRCHECK(result);
	}
	else
	{
		result = fmodSystem->getDriverCaps(0, &caps, 0, &speakermode);
		ERRCHECK(result);
		/*
		Set the user selected speaker mode.
		*/
		result = fmodSystem->setSpeakerMode(speakermode);
		ERRCHECK(result);
		if (caps & FMOD_CAPS_HARDWARE_EMULATED)
		{
			/*
			The user has the 'Acceleration' slider set to off! This is really bad
			for latency! You might want to warn the user about this.
			*/
			result = fmodSystem->setDSPBufferSize(1024, 10);
			ERRCHECK(result);
		}
		ERRCHECK(result);

		currentMute = false;
	}
	result = fmodSystem->init(100, FMOD_INIT_NORMAL, 0);
	if (result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{
		/*
		Ok, the speaker mode selected isn't supported by this soundcard. Switch it
		back to stereo...
		*/
		result = fmodSystem->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
		ERRCHECK(result);
		/*
		... and re-init.
		*/
		result = fmodSystem->init(100, FMOD_INIT_NORMAL, 0);
	}
	ERRCHECK(result);

	playSound = NULL;
}

CFMod::~CFMod()
{
	if (playSound)		playSound->release();
	if (fmodSystem)		fmodSystem->close();
}

FMOD::Sound* CFMod::FModPlaySound(char* sound, bool repeat)
{
	fmodSystem->createSound(sound, FMOD_LOOP_NORMAL, 0, &playSound);

	fmodSystem->playSound(FMOD_CHANNEL_FREE, playSound, false, &channel);

	return playSound;
}

bool CFMod::Mute(bool mute)
{
	channel->setMute(mute);
	currentMute = mute;
	return mute;
}